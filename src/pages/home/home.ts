import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SettingsPage } from '../settings/settings';
import { UserProvider, Credentials } from '../../providers/user/user';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  title = "Peerclub";
  
  user: Credentials = {};

  constructor(public navCtrl: NavController,
              public userProv: UserProvider) {
    console.log( this.userProv.user );
    this.user = this.userProv.user;
  }

  goSettingsPage():void {
  	this.navCtrl.push(SettingsPage);
  }

}
