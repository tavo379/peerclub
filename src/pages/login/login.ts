import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';

import { UserProvider } from '../../providers/user/user';
import { HomePage } from '../home/home';

import { Platform } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private afAuth: AngularFireAuth,
              public userProv: UserProvider,
              private fb: Facebook, 
              private platform: Platform) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  signInWithFacebook() {

    if (this.platform.is('cordova')) {
      //cell
      this.fb.login(['email', 'public_profile']).then(res => {
        const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
        firebase.auth().signInWithCredential(facebookCredential)
          .then( user => {
            console.log(user);
            this.userProv.uploadUser(
              user.displayName, 
              user.email,
              user.photoURL,
              user.uid,
              'facebook'
            );
    
            this.navCtrl.setRoot(HomePage);
          }).catch(e => console.log('Error con el login' + JSON.stringify(e)));
      })
    } else {
      //desktop
      this.afAuth.auth
      .signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(res => {
        console.log(res);

        let user = res.user;

        this.userProv.uploadUser(
          user.displayName, 
          user.email,
          user.photoURL,
          user.uid,
          'facebook'
        );

        this.navCtrl.setRoot(HomePage);
      });
    }
   
  }


}
