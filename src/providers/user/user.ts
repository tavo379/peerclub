import { Injectable } from '@angular/core';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  user: Credentials = {};

  constructor() {}

  uploadUser( name: string,
              email: string,
              image: string,
              uid: string,
              provider: string ) {
    this.user.name = name;
    this.user.email = email;
    this.user.image = image;
    this.user.uid = uid;
    this.user.provider  = provider;        
  }
}


export interface Credentials {
  name?: string;
  email?: string;
  image?: string;
  uid?: string;
  provider?: string;
}